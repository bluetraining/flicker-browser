package arthurabreu.com.flickerbrowser;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements GetFlickerJsonData.OnDataAvailable,
                            RecyclerItemClickListener.OnRecyclerClickListener{
    private static final String TAG = "MainActivity";
    private FlickrRecyclerViewAdapter mFlickrRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activateToolbar(false);                                                                     //This is the main activity and no other activities are calling this one so thats why we use false

        //RecyclerView class only recycle views. The data in the view to display are provided by a recycle adapter, the actual layout is provided by a layout manager and the view themselves
        //live in a view holder. So by delegating responsibility like this the recycler view becomes far more flexible than the list view and it also consequently performes a lot better.
        //fortunately we don't need to create our own layout manager, we just create a new linear layout manager object and tell the recycler view to use that (line 31).
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));                               //the layout manager is the one recycling the views

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, this)); //we can pass this as both the context because it's an activity and the listener, because we implemented the required interface

        //Create a new instance of our recycle view adapter and associate with the recycler view by calling its set adapter method (line 36). All there is left to do at this point
        //its to put our data into the adapter. The place to do that is when we receive new data, which happens in the onDataAailable method.
        mFlickrRecyclerViewAdapter = new FlickrRecyclerViewAdapter(this, new ArrayList<Photo>());
        recyclerView.setAdapter(mFlickrRecyclerViewAdapter);


        /* Testing the Get Raw Data class
        GetRawData getRawData = new GetRawData(this);
        getRawData.execute("https://api.flickr.com/services/feeds/photos_public.gne?tags=android,nougat,sdk&tagmode=any&format=json&nojsoncallback=1");                     //pass the URL to the object
        */
        Log.d(TAG, "onCreate: ends");
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: starts");
        super.onResume();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());   //create a shared preference
        String queryResult = sharedPreferences.getString(FLICKR_QUERY, "");                         //use the getstring method to read the search string from the stored shared pref. The "" is to return an empty string
                                                                                                    //instead of null in case there is nothing stored in the search activity shared pref.

        //Check if there's anything to download first instead of trying to download
        if (queryResult.length() > 0){
            GetFlickerJsonData getFlickrJsonData = new GetFlickerJsonData(this, "https://api.flickr.com/services/feeds/photos_public.gne", "en-us", true);    //pass the actual url for downloading the data, photo, etc.
//        getFlickrJsonData.executeOnSameThread("android, nougat");                                   //set the search criteria for the url as android, nougat
            getFlickrJsonData.execute(queryResult);                                                 //this is the query returned from shared pref in the search act, which contains the user data to be searched
        }
        Log.d(TAG, "onResume: ends");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        Log.d(TAG, "onCreateOptionsMenu() returned: " + true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        //This code make the search button, when clicked, launch a new activity, in this case the
        //Search Activity (class) we created in the code.
        if (id == R.id.action_search){
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
            return true;
        }
        Log.d(TAG, "onOptionsItemSelected() returned: returned");
        return super.onOptionsItemSelected(item);
    }


    //Method to see how things work
    @Override
    public void onDataAvailable (List<Photo> data, DownloadStatus status){
        Log.d(TAG, "onDataAvailable: starts");
        if (status == DownloadStatus.OK){
            mFlickrRecyclerViewAdapter.loadNewData(data);                                           //data is the data we downloaded from the async task
        }else {
            // download or processing failed
            Log.e(TAG, "onDataAvailable failed with status " + status);
        }
        Log.d(TAG, "onDataAvailable: ends");
    }

    @Override
    public void onItemClick(View view, int position) {
        Log.d(TAG, "onItemClick: starts ");
        Toast.makeText(MainActivity.this, "Normal Tap at position " + position, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemLongClick(View view, int position) {
        Log.d(TAG, "onItemLongClick: starts");
        Intent intent = new Intent(this, PhotoDetailActivity.class);
        //create new intent. this as the context, because this class extends base activity which extends AppCompat activity
        //so our Main Act is an activity and can be used whenever a context is required. The 2nd param is the activity class
        //that we want to launch.

        intent.putExtra(PHOTO_TRANSFER, mFlickrRecyclerViewAdapter.getPhoto(position));
        //putExtra method tell the PhotoDetailActivity which photo it should display, by adding data to the intent. putExtra can be used to add simple data such as int and strings but can be used to send more complex
        //objects, in this case one of our photo objects. The object needs to be serializable though. Looks like the bundle we used when saving the activity state for when it's restored after an orientation change.
        // You need to provide a key and a value then whatever needs to access the data uses that same key (PHOTO_TRANSFER) to retrieve it. That's why we defined the PHOTO_TRANSFER in the base activity class.
        // Both Main Act and PhotoDetail Act need to use the same key so we use a constant to make sure we don't type the key different in one class.
        //Serializable objects means to convert its state to a byt stream so that the byte stream can be reverted back into a copy of the object. So if an object is serializable it can be stored and retrieved. So a byte
        //stream can be saved to disk or held in memory. This work here because the implemented Serializable in the Photo class.

        startActivity(intent);
    }
}
