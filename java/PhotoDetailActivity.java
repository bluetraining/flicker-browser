package arthurabreu.com.flickerbrowser;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class PhotoDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);

        activateToolbar(true);

        Intent intent = getIntent();
        Photo photo = (Photo) intent.getSerializableExtra(PHOTO_TRANSFER);
        if (photo != null){
            TextView photoTitle = (TextView) findViewById(R.id.photo_title);
            /*
                *Defines a resource. Then define a string called text that uses the resources.getString
                * method to get the String with the ID photo_title_text from the strings.xml file.
                * The get string method will replace any placeholders with the actual value which
                * we get by retrieving the title field of our photo objects. So the gets string method
                * takes care of replacing the place holders ("Title: " + photo.getTitle()) with the
                * values that we specify. Its the same mechanism as the string.format method used in
                * the TOP 10 downloader app to display whether we are showing the top 10 or 25 results.
             */
            Resources resources = getResources();
            String text = resources.getString(R.string.photo_title_text, photo.getTitle());
            photoTitle.setText(text);
//            photoTitle.setText("Title: " + photo.getTitle());                                     //this gets a warning of placeholders

            TextView photoTags = (TextView) findViewById(R.id.photo_tags);
            photoTags.setText(resources.getString(R.string.photo_tags_text, photo.getTags()));      //this line is more concise than lines 33-35 but do the same.
//            photoTags.setText("Tags: " + photo.getTags());

            TextView photoAuthor = (TextView) findViewById(R.id.photo_author);
            photoAuthor.setText(photo.getAuthor());

            ImageView photoImage = (ImageView) findViewById(R.id.photo_image);
            Picasso.with(this).load(photo.getLink())                                                //picasso.with is used the retrieve a picasso object. load loads an image from an url and we stored the thumbnail url in the image field of the photo class
                    .error(R.drawable.placeholder)                                                  //use the image placeholder as the error image
                    .placeholder(R.drawable.placeholder)                                            //image placeholder as the place holder
                    .into(photoImage);                                                              //into is were we store the downloaded image into the image view widget in the view holder. photoImage is the widget.

        }
    }

}
