package arthurabreu.com.flickerbrowser;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by arthurabreu on 11/28/2016.
 */

class RecyclerItemClickListener extends RecyclerView.SimpleOnItemTouchListener {
    private static final String TAG = "RecyclerItemClickListen";

    /*All the approaches to implemente a recycler view listener use a callback mechanism which is
     * the usual way o notifying an activity that something's been clicked. So we need to define
     * an interface that we can use to call back
     */
    interface OnRecyclerClickListener{
        void onItemClick(View view, int position);
        void onItemLongClick(View view, int position);
    }

    private final OnRecyclerClickListener mListener;
    private final GestureDetectorCompat mGestureDetector;                                     //We use GestureDetectorCompat so that out app is compatible with earlier Android versions.

    public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, OnRecyclerClickListener listener) {
        mListener = listener;

        //This is very similar to adding an onclicklistener to a button but we're creating an anonymous class
        //that extends simple on gesture listener for the second parameter, so that we can override the
        //methods that we are interested in

        /*                                                      Why is GestureDetectorCompact in a constructor?
            The constructor runs when the class is created, so a staging ground isn't a bad analogy. So when our listener leaves staging, it should be in a position to respond to taps.
            So by the time a RecyclerItemClickListener has finished being built, it has a working GestureDetector.

            Remember that the onSingleTapUp and onLongPress methods aren't actually in the constructor, even though their code is typed in there. Those two methods are part of a GestureDetector that we could have
            created in a separate file, by extending GestureDetectorCompat.

            We've done a similar thing with button click listeners in Activity onCreate methods. The onCreate method is an activity's equivalent of a constructor, it's a method that runs when the activity is created.

            (Ok, that's not a perfect analogy, because an Activity is a Java class, so it also has a default constructor).

            Another approach could be to have a startListening method that sets up the gesture detector. The calling code would have to call the startListening method before the RecyclerItemClickListener would respond to taps.
            The gesture detector still has to be set up before taps can be detected, so the constructor is a convenient place to do that.
         */
        mGestureDetector = new GestureDetectorCompat(context, new GestureDetector.SimpleOnGestureListener() {

            /*
                ON LONG PRESS AND ON SINGLE PRESS

                Using the motion event that's passed as a parameter (e) they check to see which
                view is underneath it by using the coordinates on the screen that were tapped
                (e.getX and e.getY). The findchildViewUnder method does pretty much what is says
                checks to see what was underneath the XY coordinates of the tap and returns the
                view that it found, if any. If childView isnt null and the mListener has been set
                with a valid object we log the fact and call onItemClick or onItemLongClick methods
                that we created on the main activity.
             */
            @Override
            public boolean onSingleTapUp(MotionEvent e) {                                           //monitors an user single tap on screen
                Log.d(TAG, "onSingleTapUp: starts");
                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (childView != null && mListener != null){
                    Log.d(TAG, "onSingleTapUp: calling listener.onItemClick");
                    mListener.onItemClick(childView, recyclerView.getChildAdapterPosition(childView));
                }
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {                                                //monitors a long press
                Log.d(TAG, "onLongPress: starts");

                View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (childView != null && mListener != null){
                    Log.d(TAG, "onLongPress: calling listener onItemLongClick");
                    mListener.onItemLongClick(childView, recyclerView.getChildAdapterPosition(childView));
                }
                super.onLongPress(e);
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        Log.d(TAG, "onInterceptTouchEvent: starts");
        if (mGestureDetector != null){
            boolean result = mGestureDetector.onTouchEvent(e);
            Log.d(TAG, "onInterceptTouchEvent(): returned: " + result);
            return result;
        }else{
            Log.d(TAG, "onInterceptTouchEvent(): returned false");
            return false;
        }
    }
}
