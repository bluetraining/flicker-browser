package arthurabreu.com.flickerbrowser;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

/**
 * Created by arthurabreu on 11/28/2016.
 */

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";
    static final String FLICKR_QUERY = "FLICKR_QUERY";
    static final String PHOTO_TRANSFER = "PHOTO_TRANSFER";

    //Show the toolbar and allow activity to choose whether the toolbar should have the home button
    //showing or not
    void activateToolbar(boolean enableHome){
        Log.d(TAG, "activateToolbar: starts");
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null){
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

            if (toolbar != null){
                setSupportActionBar(toolbar);
                actionBar = getSupportActionBar();
            }
        }

        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(enableHome);
        }
    }//END METHOD

    /*
        *The method uses the get support action bar (line 21) method to get a reference to the action bar
        * so that we can add to it. Now provided that there is an action bar (22) we inflate the toolbar
        * from the toolbar xml file (line 23) then we use the set support action bar (26) method
        * with our inflated toolbar to put the toolbar in place at the top of the screen. The
        * actionBar will automatically add the home button if we tell it to, so we then get a reference
        * to the new actionbar and called set display home (32) to either enable or disable the
        * home button depending on the parameter contents that was passed to this method.
     */
}
