package arthurabreu.com.flickerbrowser;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by arthurabreu on 11/28/2016.
 */

class FlickrRecyclerViewAdapter extends RecyclerView.Adapter<FlickrRecyclerViewAdapter.FlickrImageViewHolder> {
    private static final String TAG = "FlickrRecyclerViewAdapt";
    private List<Photo> mPhotosList;                                                                //MainActivity will provide the list of photos, which will be stored here
    private Context mContext;

    public FlickrRecyclerViewAdapter(Context context, List<Photo> photosList) {
        mContext = context;
        mPhotosList = photosList;
    }

    @Override
    public FlickrImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Called by the layout manager when it needs a new view
        Log.d(TAG, "onCreateViewHolder: new view requested");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.browse, parent, false);
        return new FlickrImageViewHolder(view);
    }

    //called by the recycler view when wants new data stored in the view holder so that it can display it.
    // As the item scrow up the screen, the recycler view will provide a recycler view hold object and tell us
    //the position of data object that it needs to display. What we have to do in this method is get that item
    //from the list and put its values into the view holder widget.
    @Override
    public void onBindViewHolder(FlickrImageViewHolder holder, int position) {
        // Called by the layout manager when it wants new data in an existing row

        //Check if there is photos matching the search criteria of the user.
        if ((mPhotosList == null) || (mPhotosList.size() == 0)){
            holder.thumbnail.setImageResource(R.drawable.placeholder);
            holder.title.setText(R.string.empty_photo);                                             //Show the message on the string resource xml
        }else{

            Photo photoItem = mPhotosList.get(position);                                                //retrieve the current photo from the list
            Log.d(TAG, "onBindViewHolder: " + photoItem.getTitle() + "--->" + position);                //LOG
            Picasso.with(mContext).load(photoItem.getImage())                                           //picasso.with is used the retrieve a picasso object. load loads an image from an url and we stored the thumbnail url in the image field of the photo class
                    .error(R.drawable.placeholder)                                                      //use the image placeholder as the error image
                    .placeholder(R.drawable.placeholder)                                                //image placeholder as the place holder
                    .into(holder.thumbnail);                                                            //into is were we store the downloaded image into the image view widget in the view holder. thumbnail is the widget.

            holder.title.setText(photoItem.getTitle());                                                 //put the title into the text view title and we're done
        }
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: called");
        //returns the number of photos in the list. ? checks the condition and returns the first thing (before it) if true and the second thing (after it) if false
        return ((mPhotosList != null) && (mPhotosList.size() != 0) ? mPhotosList.size() : 1);       //If there are no records the 1 makes the message in the method above show
    }

    void loadNewData(List<Photo> newPhotos){
        mPhotosList = newPhotos;
        notifyDataSetChanged();                                                                     //tells the recicler view that the data has changed
    }

    public Photo getPhoto(int position){
        return ((mPhotosList != null) && (mPhotosList.size() != 0) ? mPhotosList.get(position) : null); //if the list isnt null AND has at least one item, then we're gonna return that requested item. Otherwise we're going to return null
    }

    static class FlickrImageViewHolder extends RecyclerView.ViewHolder{                             //class is similar to the one in the top 10 downloader app
        private static final String TAG = "FlickrImageViewHolder";
        ImageView thumbnail = null;
        TextView title = null;

        public FlickrImageViewHolder(View itemView) {
            super(itemView);
            Log.d(TAG, "FlickrImageViewHolder: starts");
            this.thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            this.title = (TextView) itemView.findViewById(R.id.title);
        }
    }


}
