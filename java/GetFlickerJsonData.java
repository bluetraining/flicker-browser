package arthurabreu.com.flickerbrowser;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arthurabreu on 11/28/2016.
 */

class GetFlickerJsonData extends AsyncTask<String, Void, List<Photo>> implements GetRawData.OnDownloadComplete {
    private static final String TAG = "GetFlickerJsonData";

    private List<Photo> mPhotoList = null;                                                          //list of photo objects
    private String mBaseURL;                                                                        //raw URL
    private String mLanguage;
    private boolean mMatchAll;                                                                      //choose between all the search terms

    //Field to store the callback object and define an interface
    private final OnDataAvailable mCallBack;
    private boolean runningOnSameThread = false;

    interface OnDataAvailable {
        void onDataAvailable(List<Photo> data, DownloadStatus status);
    }

    //CONSTRUCTOR
    public GetFlickerJsonData(OnDataAvailable callBack, String baseURL, String language, boolean matchAll) {
        Log.d(TAG, "GetFlickerJsonData: called");
        mCallBack = callBack;
        mBaseURL = baseURL;
        mLanguage = language;
        mMatchAll = matchAll;
    }

    void executeOnSameThread(String searchCriteria){
        Log.d(TAG, "executeOnSameThread: starts");
        runningOnSameThread = true;
        String destinationUri = createUri(searchCriteria, mLanguage, mMatchAll);                    //URL that contains the link to flickr

        GetRawData getRawData = new GetRawData(this);
        getRawData.execute(destinationUri);
        Log.d(TAG, "executeOnSameThread: ends");
    }

    @Override
    protected void onPostExecute(List<Photo> photos) {
        Log.d(TAG, "onPostExecute: starts");

        if (mCallBack != null){
            mCallBack.onDataAvailable(mPhotoList, DownloadStatus.OK);
        }
        Log.d(TAG, "onPostExecute: ends");
    }

    @Override
    protected List<Photo> doInBackground(String... params) {
        Log.d(TAG, "doInBackground: starts");
        String destinationUri = createUri(params[0], mLanguage, mMatchAll);
        GetRawData getRawData = new GetRawData(this);
        getRawData.runInSameThread(destinationUri);                                                 //runInSameThread is a method that does the same as execute. Execute can't be used here because there is already a async task doInBackground being executed on GetRawData
        Log.d(TAG, "doInBackground: ends");
        return mPhotoList;
    }

    private String createUri(String searchCriteria, String language, boolean matchAll){
        Log.d(TAG, "createUri: starts");

        /*Uri uri = uri.parse(mBaseURL);
        Uri.Builder builder = uri.buildUpon();
        builder = builder.appendQueryParameter("tags", searchCriteria);
        builder = builder.appendQueryParameter("tagmods", matchAll ? "ALL" : "ANY");
        builder = builder.appendQueryParameter("lang", language);
        builder = builder.appendQueryParameter("format", "json");
        builder = builder.appendQueryParameter("nojsoncallback", "1");
        uri = builder.build();*/

        //The lines below could be write like this one above

        return Uri.parse(mBaseURL).buildUpon()
                .appendQueryParameter("tags", searchCriteria)
                .appendQueryParameter("tagmode", matchAll ? "ALL" : "ANY")                          //if matchAll = true return the text "ALL". If false, return "ANY"
                .appendQueryParameter("lang", language)
                .appendQueryParameter("format", "json")
                .appendQueryParameter("nojsoncallback", "1")
                .build().toString();
    }

    @Override
    public void onDownloadComplete(String data, DownloadStatus status) {
        Log.d(TAG, "onDownloadComplete: starts. Status = " + status);

        if (status == DownloadStatus.OK){
            mPhotoList = new ArrayList<>();                                                         //Initialize the list to be ready to receive the data

            try{
                JSONObject jsonData = new JSONObject(data);
                JSONArray  itemsArray = jsonData.getJSONArray("items");                             //Array itemsArray gets the array tagged ITEMS in the url. Will look into the tag ITEMS in the json url, which contains the images

                for (int i=0; i<itemsArray.length(); i++){
                    JSONObject jsonPhoto = itemsArray.getJSONObject(i);                             //enable us to get a particularly array entry
                    String title = jsonPhoto.getString("title");                                    //the entry we will get is under the tag title in the URL
                    String author = jsonPhoto.getString("author");
                    String authorId = jsonPhoto.getString("author_id");
                    String tags = jsonPhoto.getString("tags");

                    JSONObject jsonMedia = jsonPhoto.getJSONObject("media");
                    String  photoUrl = jsonMedia.getString("m");                                    //grabs the photoUrl with the tag m

                    String link = photoUrl.replaceFirst("_m.", "_b.");                              //photos on the url json data with _m at the end, before the .jpg means that the image is in small size. _b is a large size. Here our code change the small img to a large one Source: https://www.flickr.com/services/api/misc.urls.html

                    Photo photoObject = new Photo(title, author, authorId, link, tags, photoUrl);   //Creates new photo object to hold the actual image
                    mPhotoList.add(photoObject);                                                    //add the image to our array list

                    Log.d(TAG, "onDownloadComplete: complete" + photoObject.toString());
                }//END FOR
            }catch(JSONException jsone){
                jsone.printStackTrace();
                Log.e(TAG, "onDownloadComplete: Error processing JSON data " + jsone.getMessage());
                status = DownloadStatus.FAILED_OR_EMPTY;
            }//END CATCH
        }//END IF

        if(runningOnSameThread && mCallBack != null){                                               //notify the calling class everything is done
            // now inform the caller that processing is done ~ possibly returning null there
            // was an error
            mCallBack.onDataAvailable(mPhotoList, status);
        }
        Log.d(TAG, "onDownloadComplete: ends");
    }//END METHOD

}//END CLASS
