package arthurabreu.com.flickerbrowser;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

enum DownloadStatus {IDLE, PROCESSING, NOT_INITIALIZED, FAILED_OR_EMPTY, OK};                       //IDLE: its not processing anything at this point, PROCESSING: at this stage is downloading the data, NOT_INITILIZED:
                                                                                                    //we didn't got a valid url to download, FAILED: failed to download anything or the data come back empty, OK: download successful

/**
 * Created by arthurabreu on 11/11/2016.
 * This class download the data from any URL we give it. Will do the download for us, simple as that.
 */

class GetRawData extends AsyncTask<String, Void, String>{                                           //Provides a string, which will be the url to download from and get a string back that contains the data downloaded
    private static final String TAG = "GetRawData";

    private DownloadStatus mDownloadStatus;                                                         //field to track our download status. m is member variable used to highlight variables but Android Studio does that already highlighting vars in purple
    //private final MainActivity mCallback;                                                         //declared a MainActivity object so we can pass a main activity to the constructor
    private OnDownloadComplete mCallback;                                                           //Interface

   /*
   * So, what's the problem we're trying to solve? Well, in Tim's previous course there was a DownloadData class that's been used by thousands of students over a few years, so it's thoroughly tested and proven to be reliable.
   * That's a bit like the Java classes that we use in our programs - they've had decades of use by thousands - maybe millions - of developers so if there were any bugs, they'd have been found and fixed.

   * It would be great to be able to re-use a class like that whenever we wanted to download some data to process. The problem though, is that the Top 10 Downloader's DownloadData class calls the XML parser after downloading.
   * So it can only be used to download data that's going to be parsed by that particular parser. That means it can only be used to download XML that matches the Apple RSS feed. We can't just create instances of that GetRawData
   * class to download JSON data, for example. if we did, the class would send it to an XML parser and the app would crash.

   * The solution is to decouple the downloading and the parsing. So GetRawData performs the downloading of data and nothing else. It doesn't call a parser, it just downloads data without caring whether it's XML, JSON or any other
    * format we might use it for.

   * There's no point having a class that just downloads data and hangs onto it. It doesn't matter how good it is at downloading, if it doesn't let something know when it's finished, and provide the data that it's downloaded, then it's no use to anyone.

   * So what's happening at that point in the video is that we create a callback method - a bit like a button click listener - that the GetRawData class can call once it's downloaded all the data. The method's called onDownloadComplete,
   * and it gets passed a string containing the data that was downloaded.

   * It's then up to MainActivity to do whatever it needs to with that data. GetRawData has finished its task and isn't interested in what happens to the data.

   * MainActivity in this app will call a JSON parser, but in a different app it could pass the data to an XML parser, or to a spell checker or translation class or whatever the app needs.

   * By decoupling the GetRawData class from any other logic in the app, we end up with a specialised class that can be used whenever we need to download data.
   */

    //DEFINING AN INTERFACE
    //So basically the GetRawData class defines an interface which is saying, that any class that implements this interface, must implement a method called OnDownloadComplete with the String parameter data, and DownloadStatus enem reference.
    interface OnDownloadComplete{                                                                   //Now we can change line 30 to line 31
        void onDownloadComplete(String data, DownloadStatus status);
    }

    //CONSTRUCTOR
    public GetRawData(OnDownloadComplete callback) {
        mDownloadStatus = DownloadStatus.IDLE;                                                      //initialize the object
        mCallback = callback;                                                                       //callback main activity object passed as argument is related to the one created before the constructor
    }

    void runInSameThread(String s){
        Log.d(TAG, "runInSameThread: starts");
        if (mCallback != null){
           // String result = doInBackground(s);
           // mCallback.onDownloadComplete(result, mDownloadStatus);
            mCallback.onDownloadComplete(doInBackground(s), mDownloadStatus);
        }
        Log.d(TAG, "runInSameThread: ends");
    }

    @Override
    protected void onPostExecute(String s) {
        Log.d(TAG, "onPostExecute: parameter = " + s);
        if (mCallback != null){
            mCallback.onDownloadComplete(s, mDownloadStatus);
        }
        Log.d(TAG, "onPostExecute: ends");
    }

    @Override
    protected String doInBackground(String... strings) {
        HttpsURLConnection connection = null;
        BufferedReader reader = null;

        if (strings == null){                                                                       //check if we have been given an URL when the method is called
            mDownloadStatus = DownloadStatus.NOT_INITIALIZED;                                       //if not the downloadstatus is updated to NOT_INITIALISED
            return null;
        }

        try{
            mDownloadStatus = DownloadStatus.PROCESSING;                                            //set the downloadstatus to processing
            URL url = new URL(strings[0]);                                                          //attempt to create an URL from the strings parameter. 0 indicate that its the first element of the array, as it can have more than one URL passed (elipses)
                                                                                                    //catch related: catch (MalformedURLException e)

            connection = (HttpsURLConnection) url.openConnection();                                  //URL is then used to open a connection
            connection.setRequestMethod("GET");                                                     //tell the connection that we are going to use a HTTP get connection. There are different HTTP request methods used when transferring data from the internet
                                                                                                    //so is good pratice to specify the type of request you'll be making
            connection.connect();                                                                   //connect
            int response = connection.getResponseCode();                                            //response code
            Log.d(TAG, "doInBackground: The response code was " + response);                        //log the response code

            StringBuilder result = new StringBuilder();

            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));        //set a buffer reader to read data from the input stream adding that to the string builder as basically going through all the data till we got no more data to read

            String line;
            while (null != (line = reader.readLine())){                                             //read a line at a time. While the line read is not null. the reason for null before is to get attention from who's reading the code, so they pay attention to this part
                //could be written as
                //for (String line; line != null; line = reader.readLine()){
                result.append(line).append("\n");                                                   //the result stringbuilder will get the line value and jump another line
            }

            mDownloadStatus = DownloadStatus.OK;                                                    //if no exceptions were thrown, meaning no errors found, we set the downloadstatus to OK
            return result.toString();                                                               //and return the result stringbuilder converting it to string, with the values inside the url


        }catch (MalformedURLException e){
            Log.e(TAG, "doInBackground: Invalid URL" + e.getMessage());                             //catch error if the URL is invalide. The app doesn't crash and we get this error on the logcat
        }catch (IOException e){
            Log.e(TAG, "doInBackground: IOException reading data" + e.getMessage());
        }catch (SecurityException e){
            Log.e(TAG, "doInBackground: Security Exception" + e.getMessage());
        }finally {                                                                                  //finally is always executed in a try block, whether there is an exception or not. So its a good place to close the connection (line 85) and close the bufferreader
                                                                                                    //(line 89).
            if (connection != null){
                connection.disconnect();
            }
            if (reader != null){
                try{
                    reader.close();
                }catch (IOException e){                                                             //the close can have an exception so thats why there is one here.
                    Log.e(TAG, "doInBackground: Error closing stream" + e.getMessage());
                }
            }
        }//END FINALLY
        mDownloadStatus = DownloadStatus.FAILED_OR_EMPTY;                                           //If there was any exception it would happen before line 63 and the try block would know that and won't go back trying to execute the return. But if there was an
                                                                                                    //exception we will get to the code after the catch blocks so we set the status of download to FALIED OR EMPTY and return null
        return null;
    }//END DO IN BACKGROUND
}
