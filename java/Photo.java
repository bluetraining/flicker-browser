package arthurabreu.com.flickerbrowser;

import java.io.Serializable;

/**
 * Created by arthurabreu on 11/28/2016.
 */

class Photo implements Serializable{

    private static final long serialVersionUID = 1L;

    private String mTitle;
    private String mAuthor;
    private String mAuthorID;
    private String mLink;
    private String mTags;
    private String mImage;

    //CONSTRUCTOR
    public Photo(String title, String author, String authorID, String link, String tags, String image) {
        mTitle = title;
        mAuthor = author;
        mAuthorID = authorID;
        mLink = link;
        mTags = tags;
        mImage = image;
    }

    //GETTERS
     String getTitle() {        return mTitle;    }

     String getAuthor() {        return mAuthor;    }

     String getAuthorID() {        return mAuthorID;    }

     String getLink() {        return mLink;    }

     String getTags() {        return mTags;    }

     String getImage() {        return mImage;    }

    @Override
    public String toString() {
        return "Photo{" +
                "mTitle='" + mTitle + '\'' +
                ", mAuthor='" + mAuthor + '\'' +
                ", mAuthorID='" + mAuthorID + '\'' +
                ", mLink='" + mLink + '\'' +
                ", mTags='" + mTags + '\'' +
                ", mImage='" + mImage + '\'' +
                '}';
    }
}//END CLASS
