package arthurabreu.com.flickerbrowser;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.widget.SearchView;

public class SearchActivity extends BaseActivity {
    private static final String TAG = "SearchActivity";

    private SearchView mSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: starts");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        activateToolbar(true);
        Log.d(TAG, "onCreate: ends");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOptionsMenu: starts");
        getMenuInflater().inflate(R.menu.menu_search, menu);

        /*
            * Line 49
            *The search manager provides access to the system search services and a way to get a
            *search manager instance is to call getsystemservice with a search_service constant to
            * identify which service we want. The documentation says you shouldn't attempt to create
            * search manager objects yourself.
            *
            * Line 50
            * We then get a reference to the search view widget that's embedded in the search menu
            * item of the toolbar. Retrieving that menu item is easy, we just use the find item method
            * and provide the id that we used when we created the search menu item app_bar_search
            * and we added the search view widget using the action view class property and to retrieve
            * it in code we used the get action view method.
            *
            * Line 51
            * Next we get the search manager to retrieve the searchable info from searchable.xml
            * by calling its get searchable info method and passing it the component name of the
            * activity that we want the information for.
            *
            * That searchable info is then set into the search view widget to configure it. Line 52
         */

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) menu.findItem(R.id.app_bar_search).getActionView();
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(getComponentName());
        mSearchView.setSearchableInfo(searchableInfo);
        Log.d(TAG, "onCreateOptionsMenu: " + getComponentName().toString());
        Log.d(TAG, "onCreateOptionsMenu: hint is " + mSearchView.getQueryHint());
        Log.d(TAG, "onCreateOptionsMenu: searchable info is " + searchableInfo.toString());

        mSearchView.setIconified(false);                                                            //it sets whether the widget shold appear as an icon or actually open and be ready to search. Try changing false to true to see what happens when click on the search button.

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            //This method makes the screen to return to main activity after the user enters a text in the search bar and hits enter to search
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "onQueryTextSubmit: called");
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()); //search activity store the data and MA retireve it, so we use getAppContext here instead of this
                sharedPreferences.edit().putString(FLICKR_QUERY, query).apply();                    //uses the key from base activity because this activity extends base act. the apply method stores (save) the data
                mSearchView.clearFocus();                                                           //remove focus from the toolbar before returning to MA. Otherwise the screen keeps in the search activity
                finish();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                finish();                                                                           //closes the activity and return us to the parent activity, in this case Main Activity
                return false;
            }
        });


        Log.d(TAG, "onCreateOptionsMenu: finished. Returned " + true);
        return true;
    }
}
